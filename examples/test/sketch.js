function setup() {
	// Create a new trajectory
	// Starting from 0,0
	// and heading Front (0°)
	//
	// Other heading options:
	// 90 is Right
	// -90 is Left
	// 180 is Back
	var t = new Trajectory(0,0,0);


	// Make a line
	// The Trajectory will automatically
	// create an "invisible" line from 0,0 to 0,2
	t.line(0,2, 0,10);

	// Change the "weight" of the lines
	// This can be interpreted as
	// energy level, height, movement type...
	t.strokeWeight(2);


	// The new lines will have the new weight
	t.line(0,10,10,10);

	// There's also a function to make a rectangle
	t.rect(10,10,5,15);

	// And you can also set its winding
	// (by default it is CW: Clockwise
	t.rect(10,10,5,15,"CCW");


	// Print the instructions to the console
	t.printInstructions();


	// You can also save the instructions
	// in a text file
	t.save("Instructions.txt");


}

function draw() {
  
}
