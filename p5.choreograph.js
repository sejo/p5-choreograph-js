function Trajectory(x,y,heading){

	// Starting angles
	// Heading: 
	// 0 FRONT
	// 90 RIGHT
	// -90 LEFT
	// 180 BACK

	this.position = createVector(x,y);
	this.heading = p5.Vector.fromAngle(radians(heading));

	this.color = color(0);
	this.weight = 1;

	this.strings = [];

	this.counter = 1;



	/*
	 * Creates a rectangle with top left corner in x,y, and width and height w,h
	 * Winding defined as "CCW" or "CW" 
	 * Default is CW, it can be left out
	 *
	 */
	this.rect = function(x,y,w,h,winding){
		if(winding==="CCW"){
			this.line(x,y,x,y+h);
			this.line(x,y+h,x+w,y+h);
			this.line(x+w,y+h,x+w,y);
			this.line(x+w,y,x,y);		
		}
		else{ // CW
			this.line(x,y,x+w,y);
			this.line(x+w,y,x+w,y+h);
			this.line(x+w,y+h,x,y+h);
			this.line(x,y+h,x,y);

		}
	}

	/*
	 * Changes the "weight" of the line
	 *
	 */
	this.strokeWeight = function(w){
		this.weight = w;
	}

	/* Makes a line from x1,y1 to x2,y2
	 * If x1,y1 is not the current position,
	 * first makes an "invisible" line from position to x1,y1
	 */
	this.line = function(x1,y1, x2,y2){

		var p1 = createVector(x1,y1);
		var p2 = createVector(x2,y2);


		// If starting point is different than current position

		if(p1.dist(this.position)>0.1){
			// Create "invisible" path from position to starting point
			this.createLine(this.position,p1,0);
		}

		// Create line
		this.createLine(p1,p2,this.weight);


	}

	/*
	 * Helper function to generate the line instructions
	 *
	 */
	this.createLine = function(v1,v2,weight){

		//println("pos: "+this.position.x+", "+this.position.y+" v1: "+v1.x+", "+v1.y+" v2: "+v2.x+", "+v2.y);

		var dif = p5.Vector.sub(v2,v1);

		var angleradians = dif.heading()-this.heading.heading();
		var angle = degrees(angleradians);
		var distance = dif.mag();
		var path = "";

		if (angle>=0.1 || angle<=-0.1){
			path += "Turn "+ this.angleToString(angle);
			this.pushInstruction(path);

		}

		path = "Walk "+distance.toFixed(1)+" steps ";
		path += "with a Weight of "+weight;

		this.pushInstruction(path);

		// Update position and heading
		this.position.add(dif);
		this.heading = p5.Vector.fromAngle(dif.heading());





		return path;

	}

	/*
	 * Returns a string with an angle converted to CCW or CW
	 *
	 */
	this.angleToString = function(angle){
		// Make angles stay between (-180, 180]
		if(angle<=-180){
			angle += 360;
		}
		else if(angle>180){
			angle -= 360;
		}

		var string = abs(angle).toFixed(1)+"° ";
		if(angle<0){
			string += "Counterclockwise ";
		}
		else if(angle<180){
			string += "Clockwise ";
		}

		return string;
	}


	/*
	 * Adds a new numbered instruction
	 *
	 * */
	this.pushInstruction = function(text){
		if(this.counter==1){
			this.strings.push("Start heading to the front");

			// If there's a specific starting heading
			if(this.heading.heading()!=0){
				this.strings.push("1) Turn "+this.angleToString(degrees(this.heading.heading())));
				this.counter++;
			}
		}
		var str = (this.counter) + ") "+text;
		this.counter++;
		this.strings.push(str);

	}


	/*
	 * Clears the stored strings
	 */
	this.reset = function(){
		this.strings = [];
	}

	/*
	 * Save the trajectory in a file
	 * 
	 */
	this.save = function(filename){

		this.strings.push("---");
		this.strings.push("Created with p5.choreograph.js");
		this.strings.push("Code, instructions, issues:");
		this.strings.push("https://github.com/Escenaconsejo/p5.choreograph.js");

		saveStrings(this.strings,filename);
		

	}

	/*
	 * Print the list of instructions to console
	 * 
	 */
	this.printInstructions = function(){
		for(var i=0; i<this.strings.length; i++){
			println(this.strings[i]);
		}
	}

}
