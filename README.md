# p5.choreograph.js
Render [p5.js](http://p5js.org) drawing commands as instructions for movement. 

## Description
This is a project originated in the Educate the Future Fall 2016 course in ITP - NYU.
Realizing that there is a problem of a lack of body movement in programming classes, this project attempts to solve it by making people move while programming.

The idea is to shift the usual paradigm of what can be created with creative code: graphics, text and sound. Here, instead of having an instantaneous feedback for the written drawing commands in p5.js, those commands get translated as instructions that a person can follow moving in a space. Basically the lines in the programmed drawing are converted to trajectories to walk on. 

The graphical output of the commands is omitted on purpose for several reasons:
* The person following the generated instructions has to follow them without a reference of the intended result, making its execution more authentic 
* The visual thinking of the programmed drawings has to be changed to a certain extent to a spatial thinking and to a thinking involving other person(s) in a space. 
* It is challenging and engaging 
* The feedback on the result of the created code is slowed down, and that may help thinking further what to write before actually executing it
* It allows a new way of human interaction
* The graphical output can always be drawn with the normal p5 functions 

_In progress..._

## Install
Copy the p5.choreograph.js file into your libraries folder, and add the following line to your index.html file:
```html
<script language="javascript" type="text/javascript" src="libraries/p5.choreograph.js"></script>

```

## Example
```javascript
function setup() {
	// Create a new trajectory
	// Starting from 0,0
	// and heading Front (0°)
	//
	// Other heading options:
	// 90 is Right
	// -90 is Left
	// 180 is Back
	var t = new Trajectory(0,0,0);


	// Make a line
	// The Trajectory will automatically
	// create an "invisible" line (Weight 0) from 0,0 to 0,2
	t.line(0,2, 0,10);

	// Change the "weight" of the lines
	// This can be interpreted as
	// energy level, height, movement type...
	t.strokeWeight(2);


	// The new lines will have the new weight
	t.line(0,10,10,10);

	// There's also a function to make a rectangle
	t.rect(10,10,5,15);

	// And you can also set its winding
	// (by default it is CW: Clockwise
	t.rect(10,10,5,15,"CCW");


	// Print the instructions to the console
	t.printInstructions();


	// You can also save the instructions
	// in a text file
	t.save("Instructions.txt");


}

function draw() {
  
}

```

The corresponding output for the example would be:
```
Start heading to the front
1) Turn 90.0° Clockwise 
2) Walk 2.0 steps with a Weight of 0
3) Walk 8.0 steps with a Weight of 1
4) Turn 90.0° Counterclockwise 
5) Walk 10.0 steps with a Weight of 2
6) Walk 5.0 steps with a Weight of 2
7) Turn 90.0° Clockwise 
8) Walk 15.0 steps with a Weight of 2
9) Turn 90.0° Clockwise 
10) Walk 5.0 steps with a Weight of 2
11) Turn 90.0° Clockwise 
12) Walk 15.0 steps with a Weight of 2
13) Turn 180.0° 
14) Walk 15.0 steps with a Weight of 2
15) Turn 90.0° Counterclockwise 
16) Walk 5.0 steps with a Weight of 2
17) Turn 90.0° Counterclockwise 
18) Walk 15.0 steps with a Weight of 2
19) Turn 90.0° Counterclockwise 
20) Walk 5.0 steps with a Weight of 2
---
Created with p5.choreograph.js
Code, instructions, issues:
https://github.com/Escenaconsejo/p5.choreograph.js
```

## Function reference
soon
